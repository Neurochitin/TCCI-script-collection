// Places a random painting in the world at the player's position. Good example of complex logic: shows that you can
// have scripts do more than just some simple projectile or NPC spawns.

// Paintings are represented in the game as four different tile types, each representing one size of painting. We
// select one of those here, and then later a random style (determining what the painting itself will be) for the tile
// we chose.
var tile = RandomlySelect(240, 242, 245, 246);

var placeStyle = 0;
var offsetX = 0, offsetY = 0, width = 3, height = 3;

// Find a style of painting depending on the size, and adjust offset (so the painting will be placed with the player
// at its center) and size (so we can correctly place walls behind the painting).
if(tile == 240) // 3x3 painting
{
    placeStyle = RandomlySelect(12, 13, 14, 15, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35);
    offsetX = offsetY = -1;
}

if(tile == 242) // 6x4 painting
{
    placeStyle = RandomlySelect(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36);
    offsetX = -2;
    offsetY = -2;
    width = 6;
    height = 4;
}

if(tile == 245) // 2x3 painting
{
    placeStyle = RandomlySelect(0, 1, 2, 3, 4, 5, 6);
    offsetY = -1;
    width = 2;
}

if(tile == 246) // 3x2 painting
{
    placeStyle = RandomlySelect(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
    offsetX = -1;
    height = 2;
}

// Place some walls: those are necessary for the painting to be placed on. Existing walls will not be overwritten.
var wallType = 16; // The type of wall that will be placed, here dirt. See
                   // https://github.com/tModLoader/tModLoader/wiki/Vanilla-Wall-IDs for a list of possible choices
for(int x = 0; x < width; x++) {
    for(int y = 0; y < height; y++) {
        PlaceWallAtOffset(wallType, x + offsetX, y + offsetY);
    }
}

// Actually place the painting where the player is standing.
PlaceTileAtOffset(tile, 0, 0, style: placeStyle);