// Teleports every NPC to the player position. Shows how you can iterate over all NPCs currently in the game, as well
// as that you can use Teleport() for NPCs as well.
// Note that due to the way despawning works in Terraria, there will usually be no NPCs that are very far off screen.
// (Except bosses) This may diminish the effect you want to achieve with something like this — just be aware of that.
foreach(NPC npc in Main.npc) {
    npc.Teleport(Player.position); 
}