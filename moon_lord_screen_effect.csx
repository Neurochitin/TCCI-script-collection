// Lights up the screen, as if the moon lord is spawning.
// Demonstrates how to use NextTicks to do something for a specific amount of ticks (in this case 120 = 2 seconds)
NextTicks(120, remainingTicks => {
    // Light up the screen fully
    Terraria.GameContent.Events.MoonlordDeathDrama.RequestLight(1f, Player.Center);
});

// Play the moon lord spawn sound
Main.PlaySound(29, Player.position, 92);