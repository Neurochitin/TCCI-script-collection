// Shows how to deal damage to the player in a simple way.

// Base amount of damage to deal (will be reduced by defense and damage reduction)
var damageAmount = 1;

// Terraria requires us to specify a death reason, which determines the message to display if the player dies from
// this damage. Replace "Poked to death..." with your own text if you desire.
var deathReason = PlayerDeathReason.ByCustomReason("Poked to death...");

// From which direction the player is hit; this is important for knockback purposes.
// 1 = hit from the left (player will be knocked back to the right)
// -1 = hit from the right (player will be knocked back to the left)
// Use RandomlySelect(1, -1) to randomise this if you desire.
var hitDirection = 1;

Player.Hurt(deathReason, damageAmount, hitDirection);