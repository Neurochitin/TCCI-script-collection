// Deletes the item in the helmet slot (by setting it to an empty item).
// See the Inventory section in the readme for a list of other slots you might be interested in.
Player.armor[0] = new Item();