// Use this script to simply spawn an NPC (enemy, critter or town NPC) a certain distance from where the player is standing.
// See a list of NPC IDs here: https://github.com/tModLoader/tModLoader/wiki/Vanilla-NPC-IDs
var id = 443; // The ID of the NPC to spawn (443 would be a Gold Bunny, for example)
var xDistance = 3; // Number of tiles in x direction (here 3 blocks to the right; -3 would be to the left for example)
var yDistance = -10; // Number of tiles in y direction (here 10 blocks above)
SpawnNPCAtPlayer(id, xDistance, yDistance);