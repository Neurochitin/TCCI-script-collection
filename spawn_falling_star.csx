// Drops a falling star (projectile ID 12). Falling stars are not technically affected by gravity; their apparent
// "falling" comes from the fact that they move in a fixed direction from when they spawn. By playing with the starting
// point and velocity in the method call below (for example by flipping signs), you can make "rising stars"
// or any other linear motion you desire. 
QuickProjectileAtOffset(12,         // projectile ID
                        -20, -50,   // starting point: here it comes from the left, as it does in vanilla
                        4, 10,      // velocity: 4 towards the x direction, 10 towards the y direction
                        9999        // damage to mobs upon hit (will not damage players)
                        );