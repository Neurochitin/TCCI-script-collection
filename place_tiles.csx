// Place some tiles at specified positions around the player. In this specific case, hellstone will be placed below
// the player's feet, causing them to get burned.

var tileType = 58; // Hellstone; see https://github.com/tModLoader/tModLoader/wiki/Vanilla-Tile-IDs for a list

// Two method calls means two tiles will be placed. The positions (x, y = 0, 3 and 1, 3 respectively) are counted from
// the player's upper left corner. So y = 3 will mean that the tiles will end up just below the player's feet, as the
// player is 3 blocks tall.
// "forced: true" will make the placed tiles replace existing tiles. Use "forced: false" instead if you don't want this.
PlaceTileAtOffset(tileType, 0, 3, forced: true);
PlaceTileAtOffset(tileType, 1, 3, forced: true);