// Use this script to simply spawn an NPC (enemy, critter or town NPC) where the player is standing.
// See a list of NPC IDs here: https://github.com/tModLoader/tModLoader/wiki/Vanilla-NPC-IDs
// Modded NPCs can also be spawned with this command, although it is more complicated to get the ID. The easiest way
// is probably to use the Mob Spawn Window from HERO's mod, which displays each NPC's ID as you hover over it.
var id = 443; // The ID of the NPC to spawn (443 would be a Gold Bunny, for example)
SpawnNPCAtPlayer(id);