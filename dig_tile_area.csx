// Digs out a 2 (or 3 if the player is standing on two tile boundaries) tile wide area below the player that is
// 3 blocks deep.
// This script demonstrates how for loops can be used to do repetitive tasks, as well as how "TileRightOfPlayer" can be
// used to fully iterate over all blocks the player is standing on (so the player will fall into the generated hole).

// Iterate from y = 0 (upper row of the player sprite) to 6 (3 blocks below the player's feet).
for(int y = 0; y <= 6; y++) {
    // "TileRightOfPlayer" always refers to the first tile that is, in its entirety, right of the player's position.
    // If the player stands on just two tiles, this will be very straightforward and refer to the tile that is two
    // blocks to the right of the player's top left corner. But if the player is standing on three tiles, where one tile
    // is fully covered by the player, this will refer to one tile right of that.
    // As we iterate using the "<" sign, the digging will cover the tile just before the TileRightOfPlayer, but not
    // that tile itself. So the rightmost tile the player is standing on will be removed, but not one tile further right
    // of that.
    for(int x = 0; x < TileRightOfPlayer; x++) {
        // Remove the tile, dropping it as an item
        KillTileAtOffset(x, y);
    }
}