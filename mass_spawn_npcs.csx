// This script shows how to summon a lot of NPCs at once.

var count = RandomInteger(50, 60); // Randomly generate a number between 50 and 60, to determine how many NPCs will be spawned
var npcId = 48; // Harpy

for(int i = 0; i < count; i++) {
    // Randomise the X and Y offsets to 40 tiles in all directions from the player
    SpawnNPCAtPlayer(npcId, RandomFloat(-40, 40), RandomFloat(-40, 40));
}