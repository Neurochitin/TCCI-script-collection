// Liquids use a different mechanism of placement from regular tiles. Here we place water at the player's feet.
var liquidType = Tile.Liquid_Water; // Use Liquid_Lava or Liquid_Honey accordingly.

// The rest is basically the same as in place_tiles.csx, just that "forced: true" does not apply.
// The y coordinate is 2, so the liquid will spawn as to just submerge the player's feet. 
PlaceLiquidAtOffset(liquidType, 0, 2);
PlaceLiquidAtOffset(liquidType, 1, 2);