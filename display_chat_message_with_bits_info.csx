// This script can only be used for bit donations! It is recommended to save it as "bits.csx" so it will be called for
// every bit donation.
// It displays a chat message with the donator's name and the donated amount.
Main.NewText($"{ctx.BitsInfo.Name} donated {ctx.BitsInfo.Amount} bits!");