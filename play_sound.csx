// This script can be used to just play a sound without doing anything else.
// See the "List of Sounds" in this thread for a list of sounds: https://forums.terraria.org/index.php?threads/dust-and-sound-catalogue-2.42370/
// The soundCategory refers to the category of sounds (for example "15: boss summon, dig, scream" in that thread)
// The soundStyle refers to the style (for example "0: boss summon laughter")
// The volume refers to the factor the volume will be multiplied by (1 = no change; 10 = 10x the volume, 0.5 = 0.5x
// the volume). This is important for some sounds which are naturally quiet and thus need to be boosted to be audible. 
var soundCategory = 15;
var soundStyle = 0;
var volume = 1;
Main.PlaySound(soundCategory, Player.position, soundStyle, volumeScale: (float) volume);