// This script can only be used for monetary donations! It is recommended to save it as "donations.csx" so it will be
// called for every donation.
// It displays a chat message with the donator's name and the donated amount.
Main.NewText($"{ctx.DonationInfo.Name} donated ${ctx.DonationInfo.Amount}!");