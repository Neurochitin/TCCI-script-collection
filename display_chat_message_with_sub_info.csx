// This script can only be used for subscriptions! It is recommended to save it as "subscription.csx" so it will be
// called for every time someone subscribes.
// It displays a chat message with the subber's name and the amount of months they subbed for.
var monthWord = ctx.SubInfo.Months > 1 ? "months" : "month"; // so we don't say "1 months"
Main.NewText($"{ctx.SubInfo.Name} subscribed for {ctx.SubInfo.Months} {monthWord}!");