// Iterate over a 40x40 area, removing all instances of a specific tile (here torches) in the process.

// See https://github.com/tModLoader/tModLoader/wiki/Vanilla-Tile-IDs for a list of possible tiles
var tileToRemove = 4; // Torch

// We iterate starting at 19 tiles left of the player's left edge, and end at 20 tiles right of the player's left edge.
// As 0 is counted too, this results in 40 covered tiles.
for(int xOffset = -19; xOffset <= 20; xOffset++)
{
    for(int yOffset = -19; yOffset <= 20; yOffset++)
    {
        // By replacing the comparison in this if statement with something more complicated, it is also possible to
        // remove more than one type of tile, for example.
        if(GetTileAtOffset(xOffset, yOffset).type == tileToRemove) {
            // If the tile we found is of the type we want, remove it (dropping it as an item).
            KillTileAtOffset(xOffset, yOffset);
        }
    }
}