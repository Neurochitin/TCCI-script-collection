// Shows how to use RandomlySelect to randomly generate IDs (or other information). The actual spawning logic is the
// same as in spawn_npc_simple_at_player.csx, but the ID is randomly generated here.
// As always, see https://github.com/tModLoader/tModLoader/wiki/Vanilla-NPC-IDs for a list of possible NPC IDs
var id = RandomlySelect(143, 144, 145); // Randomly select one of: Snowman Gangsta (143), Mister Stabby (144), Snow Balla (145)
SpawnNPCAtPlayer(id); // Spawn the ID we selected