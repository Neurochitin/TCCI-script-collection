// This script spawns an item at the player's position, causing them to pick it up instantly if their inventory is not
// full.
var id = 73; // Which item to spawn (here a gold coin),
             // see https://github.com/tModLoader/tModLoader/wiki/Vanilla-Item-IDs for a list of possible values
Player.QuickSpawnItem(id);