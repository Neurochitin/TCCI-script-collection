// Iterate over a 20x20 area centered on the player and replace sand with glass. Can be adapted to any range or any
// set of tiles.

for(int xOffset = -9; xOffset <= 10; xOffset++)
{
    for(int yOffset = -9; yOffset <= 10; yOffset++)
    {
        if(GetTileAtOffset(xOffset, yOffset).type == 53) // 53 = sand
        {
            // 54 = glass
            // forced: true is necessary to overwrite the existing tile.
            PlaceTileAtOffset(54, xOffset, yOffset, forced: true);
        }
    }
}