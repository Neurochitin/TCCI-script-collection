// Demonstrates giving a buff (or debuff) to the player.

// The buff to add to the player. See https://github.com/tModLoader/tModLoader/wiki/Vanilla-Buff-IDs for a list of
// possible choices
var buffId = 3; // here: Swiftness

// The time the buff should last for, in ticks. One second = 60 ticks.
var duration = 20 * 60; // 20 * 60 ticks = 20 seconds

Player.AddBuff(buffId, duration);