// This summons a boulder (which is technically a projectile) above the player. Feel free to modify the values in the
// method call as you please
QuickProjectileAtOffset(99,     // projectile ID: boulder
                        0, -10, // x/y offset: 10 blocks above player
                        0, 0,   // x/y speed: 0 speed in either direction
                        100);   // base damage: 100 (can be any value, including 0)