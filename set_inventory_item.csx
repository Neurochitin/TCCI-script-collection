// Overwrites an inventory slot with a new item. Here we set the chestplate slot to contain a piece of Silver Chainmail.
var item = new Item(); // Create a new item.
item.SetDefaults(82); // Initialise the new item from 82 = silver chainmail (https://github.com/tModLoader/tModLoader/wiki/Vanilla-Item-IDs)

// Here you can modify the item variable, as you would in spawn_item_special.csx.

// Set the chainmail slot to the item we created. See the Inventory section in the readme for a list of other slots
Player.armor[1] = item;