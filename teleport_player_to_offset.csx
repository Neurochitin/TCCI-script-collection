// Player.Teleport can be used to teleport the player to a specific position. Here we teleport the player 1.5 tiles
// down of where he is currently standing, causing them to fall through a one tile thick bridge they might be standing
// on, for example.

var xOffset = 0; // Teleport 0 tiles in the x direction
var yOffset = 1.5; // Teleport 1.5 tiles down in the y direction

// Do the teleportation
Player.Teleport(Player.position + new Vector2((float) (xOffset * 16), (float) (yOffset * 16)));