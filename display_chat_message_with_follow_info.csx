// This script can only be used for follows! It is recommended to save it as "follow.csx" so it will be
// called for every time someone follows the channel.
// It displays a chat message with the follower's name.
Main.NewText($"{ctx.Info.Name} is now following you!");