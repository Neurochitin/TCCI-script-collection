// Shows how to modify an NPC's properties after it was spawned. The game gives you a huge amount of freedom here:
// the NPC ID by itself essentially only determines basic looks, sounds, and drops of the NPC. All other properties
// (damage, health, behaviour, name, ...) can be modified relatively freely.
// See https://github.com/tModLoader/tModLoader/wiki/NPC-Class-Documentation#fields-and-properties for a list of things
// you can modify.

// Here we summon a blue slime, make it really large and powerful, and rename it to "Gigaslime".

var id = 1; // Blue Slime
var xOffset = 0, yOffset = -45; // Spawn the slime 45 blocks above the player

var npc = SpawnNPCAtPlayer(id, xOffset, yOffset);
npc.scale *= 30f; // Make it show up 30x the size
npc.Size *= 30f; // Make its hitbox 30x the size
npc.lifeMax *= 300; // Make it have 300x as much max health
npc.life *= 300; // Make its current health also be 300x the normal amount (otherwise it would be close to dying)
npc.damage *= 30; // Make it deal 30x as much damage
npc.GivenName = "Gigaslime"; // Rename to "Gigaslime"