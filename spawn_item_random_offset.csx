// Spawns a heart (which is technically an item) somewhere in a 20x20 area around the player.
// Demonstrates more complex item spawning mechanics
Item.NewItem(
    (int) (Player.position.X - RandomFloat(-10 * 16, 10 * 16)), // 10 tiles in either X direction (= 20 in total) 
    (int) (Player.position.Y - RandomFloat(-10 * 16, 10 * 16)), // 10 tiles in either Y direction (= 20 in total)
    Player.width, Player.height,
    58); // 58 = heart; replace for another other item