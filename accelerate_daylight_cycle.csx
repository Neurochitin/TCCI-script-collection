// Accelerates the passage of time to 60x the normal rate for the next 100 ticks (1.6 seconds). Should be relatively
// self explanatory.
NextTicks(100, remainingTicks => {
    Main.dayRate = 60;
    
    // If we are at the end of the 100 ticks, reset the dayRate so it will not continue like this forever.
    if(remainingTicks == 0) Main.dayRate = 1;
});