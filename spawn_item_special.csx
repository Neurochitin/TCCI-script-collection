// Like NPCs, an item's behaviour is mostly defined by default values that can be relatively freely changed later on.
// So you can make nearly any item behave like nearly any other item, or even like something completely new.
// Here we make an angel statue, then modify most of its properties so it does something else entirely.

// List of things you can change: https://github.com/tModLoader/tModLoader/wiki/Item-Class-Documentation

Item item = new Item();
item.SetDefaults(52); // 52 = Angel statue
item.createTile = -1; // Determines which tile it will place when used: -1 = can't be placed
item.scale = 3f; // 3x the size
item.maxStack = 1; // not stackable
item.rare = 10; // rarity 10 (red name; see the vanilla wiki for a list of rarities)
item.useTime = 5; // use time 5 (very fast; as fast as the S.D.M.G.)
item.holdStyle = 2; // held like a breathing reed when not in use (rotated 45° CCW)
item.autoReuse = true; // can hold to continue using item
item.consumable = false; // does not get consumed after use
item.useStyle = 1; // swings like a sword
item.knockBack = 20f; // insane knockback
item.shoot = 207; // shoots Chlorophyte Bullets...
item.shootSpeed = 1f; // ...at a really slow velocity
item.damage = 30; // 30 base damage (applies to both the swing and to the shot bullets)
item.noMelee = false; // the swing itself deals damage too
item.ranged = true; // the damage this item deals is ranged
item.pick = 100; // pickaxe power (doesn't appear to do anything in this case)
item.accessory = true; // can (also) be worn as an accessory
item.defense = 10; // provides 10 defense when equipped
item.crit = 4; // crit chance = 4
item.mana = 5; // uses 5 mana when used
item.healLife = 10; // heals 10 life when used
item.buffType = 119; // gives buff 119 (Lovestruck) when used
item.buffTime = 60; // gives 60 ticks = 1 second of the buff when used
item.mech = true; // shows wires when held
item.color = Color.Red; // coloured red
item.prefix = 5; // prefix 5 ("Sharp") - this only changes the prefix the item is displayed as, not any stats

// Spawn the item where the player is standing. Because we set some properties, we have to use QuickSpawnClonedItem
// instead of QuickSpawnItem
Player.QuickSpawnClonedItem(item);