# TCCI script collection

This repository includes a bunch of scripts for [TCCI](https://gitlab.com/Neurochitin/TCCI) that you can use to get started with the mod. Notably, it also serves as a source of trusted scripts, so you don't have to worry so much about random scripts floating around on the internet possibly being malicious.

## How to use scripts

First, make sure TCCI is installed and set up properly. (Setting up Streamlabs and Twitch connections isn't necessary to test these scripts.) Ideally, you would also be familiar with [TCCI's documentation](https://gitlab.com/Neurochitin/TCCI/-/blob/master/README.md).
 
Now select some scripts from this repository that you want to use. Once you have found one or multiple scripts, do the following steps for each script:

1. Consider when exactly the script should run in a stream (chat command? bit donation? something else?), then consult the [script naming guide](https://gitlab.com/Neurochitin/TCCI/-/blob/master/README.md#script-names) for the file name you would have to give the script, for example `commandTest.csx` or `bits400.csx`.
2. Create a file with that name in the `TCCI Scripts` folder, then copy the contents of the script you selected in.
3. Adapt variables in the script to your desires.

Once you have done all of this, go in-game and do `/tcci reload`. It should compile all the scripts, which might take a while. Then, for each script, do `/tcci testscript <script name>`, where `<script name>` is the filename without `.csx` (e.g. `bits400`), to test whether the script works at all. You can also do `/t <script name>` for faster testing if you have a lot of scripts.

## Contributing

If you have a finished script (or even just an idea) that you want to contribute, feel free to message me on Discord! Join my server (https://discord.gg/pnveUJW), or just message me directly at Neurochitin#0995. You can also just send a merge request on GitLab.