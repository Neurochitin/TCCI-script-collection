// Starts the Slime Rain.
// There are lots of similar methods and fields on the Main class that allow you to do various global things. But 
// unfortunately there isn't really a reference for everything you can do. You basically have to look through vanilla
// code if you want to do something really specific. Feel free to ask me for help if you are stuck.
Main.StartSlimeRain();